import {gCarObj} from "./info"

function App() {

  return (
    <div>
      <ul>
      {gCarObj.map(function(value, index){
        return <li key={index}>{index + 1}. {value.make} {value.model} : {value.vID} - {value.year > 2018 ? "Xe mới" : "Xe cũ"}</li>
      })}
      </ul>
      
    </div>
  );
}

export default App;
